# About DOKU
As a payment gateway, DOKU enables you to accept a wide range of different payment methods.

# Installation
```
npm install doku-api
```

# Pre-requisites
You must have a merchant account on DOKU, as you can only get your keys (Merchant ID and Shared Key) after registering. 

Register here: https://merchant.doku.com/acc/register

# Usage
After including the library, you can use `init()` function to set your keys. The API's base URL can also be set depending on `NODE_ENV` environment variable (`'production'` or `'development'`), or by using `setURL()`
```javascript
const DOKU = require('doku-api');

DOKU.init('<YOUR_MERCHANT_ID>', '<YOUR_SHARED_KEY>');
DOKU.setURL('http://staging.doku.com/api/payment'); //NOTE: No trailing slashes!
```
The usage of this library is similar to how it is originally used with PHP. Though, some function names are different, such as `doCreateWords()` became `getWords()`.

All payment functions (`doPayment()`, `doPrepayment()`, `doDirectPayment()`, `getPaycode()`) returns a Promise. So, you can use them with `async/await`.

You might want to also include a package for hashing, and install a date time library like `moment` to help you enter the required data.

# Example
Below is an example usage with ExpressJS for credit card payment using `async/await`
```javascript
const DOKU = require('doku-api');
const crypto = require('crypto');
const moment = require('moment');

router.post('/handle-cc', async function (req, res, next) {
    try {
        let basket = req.body.basket || req.query.basket;

        let words = DOKU.getWords({
            amount: Number(basket.reduce((ac, item) => ac + item.amount), 0).toFixed(2),
            invoice: req.body['doku_invoice_no'],
            currency: '360',
            pairing_code: req.body['doku_pairing_code'],
            token: req.body['doku_token']
        });

        let customer = {
            name: '<FULL_NAME>',
            data_email: '<EMAIL_ADDRESS>',
            data_phone: '<PHONE_NUM>' || '-',
            data_address: '<USER_ADDRESS>' || '-'
        };

        let date = moment().format('YYYYMMDDHHmmss').toString(); //Requires MomentJS

        let paymentData = {
            req_mall_id: req.body['doku_mall_id'],
            req_chain_merchant: 'NA',
            req_amount: Number(basket.reduce((ac, item) => ac + item.amount), 0).toFixed(2),
            req_words: words,
            req_purchase_amount: Number(basket.reduce((ac, item) => ac + item.amount), 0).toFixed(2),
            req_trans_id_merchant: req.body['doku_invoice_no'],
            req_request_date_time: date,
            req_currency: '360',
            req_purchase_currency: '360',
            req_session_id: crypto.createHash('sha1').update(date).digest('hex'),
            req_name: customer.name,
            req_payment_channel: 15,
            req_basket: mappedBasket,
            req_mobile_phone: customer.data_phone,
            req_address: customer.data_address,
            req_email: customer.data_email,
            req_token_id: req.body['doku_token']
        };

        let result = await DOKU.doPayment(paymentData); //Execute payment

        if (result.res_response_code == '0000') {
            //Payment success, do something (save to db, etc.)
        } else {
            //Handle failure
        }

        result.res_show_doku_page = 1;
        return res.json(result);
    } catch (err) {
        console.error("PAYMENT ERROR", err);
        return res.status(500).send("PAYMENT ERROR");
    }
});
```

Original documentation (in PHP): https://merchant.doku.com/acc/downloads/doc/onecheckoutv2-api/DOKU-API-MERCHANT-v1-10.pdf