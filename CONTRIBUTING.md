# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

## Reporting Bugs or Issues

Please use this project's Gitlab repo issue tracking system. Whenever a new issue is created, include error codes, screenshots, and/or debugging results if possible.