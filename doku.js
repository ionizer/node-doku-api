'use strict';

const crypto = require('crypto');
const request = require('request');

var baseURL = process.env.NODE_ENV === 'production' ? 'https://pay.doku.com/api/payment' : 'http://staging.doku.com/api/payment';
var mallID = null;
var shared = null;

/**
 * Initialize library
 * @param {string} id DOKU Mall ID (Merchant ID)
 * @param {string} sk DOKU Shared Key
 */
function init(id, sk) {
    mallID = id;
    shared = sk;
}

/**
 * Set API base URL
 * @param {string} url Target base URL
 */
function setURL(url) {
    baseURL = url;
}

/**
 * DOKU pre-payment
 * @param {Object} data See data format
 * @returns {Promise<Object>} A Promise containing JSON response from DOKU
 */
function doPrePayment(data) {
    data.req_basket = formatBasket(data.req_basket);
    return execute(baseURL + "/PrePayment", data);
}

/**
 * DOKU Payment
 * @function doPayment
 * @param {Object} data See data format
 * @returns {Promise<Object>} A Promise containing JSON response from DOKU
 */
function doPayment(data) {
    data.req_basket = formatBasket(data.req_basket);
    return execute(baseURL + "/paymentMip", data);
}

/**
 * DOKU Direct Payment
 * @param {Object} data See data format
 * @returns {Promise<Object>} A Promise containing JSON response from DOKU
 */
function doDirectPayment(data) {
    return execute(baseURL + "/PaymentMIPDirect", data);
}

/**
 * DOKU Generate Paycode
 * @param {Object} data See data format
 * @returns {Promise<Object>} A Promise containing JSON response from DOKU
 */
function getPaycode(data) {
    return execute(baseURL + "/doGeneratePaymentCode", data);
}

/**
 * Create words for DOKU API
 * @function getWords
 * @param {Object} data payment data - see data format
 * @returns {string} hashed words
 */
function getWords(data) {
    let s = this.getWordsRaw(data);

    return crypto.createHash('sha1').update(s).digest('hex');
}

/**
 * Create raw words
 * @function getWordsRaw
 * @param {Object} data payment data - see data format
 * @returns {string} concatenated data
 */
function getWordsRaw(data) {
    let s = '';

    if (data.hasOwnProperty('device_id'))
        if (data.hasOwnProperty('pairing_code')) s = data.amount + mallID + shared
            + data.invoice + data.currency + data.token + data.pairing_code + data.device_id;
        else s = data.amount + mallID + shared
            + data.invoice + data.currency + data.device_id;
    else if (data.hasOwnProperty('pairing_code')) s = data.amount + mallID + shared
        + data.invoice + data.currency + data.token + data.pairing_code;
    else if (data.hasOwnProperty('currency')) s = data.amount + mallID + shared
        + data.invoice + data.currency;
    else s = data.amount + mallID + shared + data.invoice;

    return s;
}

/** 
 * Execute request to DOKU API
 * @function execute
 * @param {string} url DOKU's URL
 * @param {Object} payload data to be sent
 * @returns {Promise<Object>} A Promise containing JSON response from DOKU
 * @private
 */
function execute(url, payload) {
    //console.log("PAYMENT PAYLOAD", payload);
    if (mallID === null || shared === null) throw new Error("Both MALL ID and SHARED KEY are required");
    if (payload.req_mall_id != mallID) throw new Error("Unauthorized MallID");
    return new Promise((resolve, reject) => {
        request.post(url, { form: { data: JSON.stringify(payload) } }, (err, res, body) => {
            if (err) reject(new Error(err));
            else if (res.statusCode >= 200 || res.statusCode < 300) resolve(JSON.parse(body));
            else reject(new Error("URL: " + url + " responded with HTTP " + res.statusCode));
        });
    });
}

/**
 * Format basket into DOKU's format
 * @function formatBasket
 * @param {Object[]} basket Array of JSON
 * @param {string} basket[].name Item name
 * @param {number} basket[].amount Payment amount
 * @param {number} basket[].quantity Item quantity
 * @param {number} basket[].subtotal Payment subtotal
 * @private
 */
function formatBasket(basket) {
    let parsed = '';

    basket.forEach(item => {
        parsed += item.name + ','
            + Number(item.amount).toFixed(2) + ','
            + item.quantity + ','
            + Number(item.subtotal || (item.amount * item.quantity)).toFixed(2) + ';';
    });

    return parsed;
}

module.exports = {
    init, setURL, doPrePayment, doPayment, doDirectPayment, 
    getPaycode, getWords, getWordsRaw
}